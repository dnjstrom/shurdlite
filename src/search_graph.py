from pdb import set_trace
from collections import deque
import itertools
import heapq

class SearchGraph(object):
    """ A SearchGraph is an graph with nodes containing state and
        edges denoting operations on the state. The graph is used to find
        a sequence of operations to transform one state to another.

        All states used in the graph must have a valid string representation.

        Parameters
        ----------
        validator   A function which takes a state, and returns true or false
                    depending on whether the goal state has been found.
        substates   A function which takes a state and returns a tupels of possible
                    actions and the resulting child states.
        objects     A dictionary of the definition of the objects in the graph
        options     User options for tuning the search behavior.
        """

    def __init__(self, validator, substates,cost_calculator, objects, options={}):
        super(SearchGraph, self).__init__()

        self.validator = validator
        self.substates = substates
        self.cost_calculator = cost_calculator
        self.objects = objects
        self.opt = self.get_options(options)
        self.queue = PriorityQueue()
        self.found = set()

    def get_options(self, user_options):
        standard_options = {
            'max_depth' : 15
        }
        return standard_options.update(user_options)

    def add_root_state(self, state):
        self.queue.push(Node(self, [], state, 0, 0))

    def search(self):
        while not self.queue.is_empty():
            path = self.queue.pop().search()
            if path is not None: return path


class Node(object):
    """Maintains a state and the path to that state."""
    def __init__(self, graph, path, state, cost, movements):
        super(Node, self).__init__()

        self.graph = graph
        self.path = path
        self.state = state
        self.cost = cost
        self.movements = movements #Robot arm movements in y-axis


    def search(self):
        if str(self.state) in self.graph.found:
            return
        else:
            self.graph.found.add(str(self.state))
            
        if self.graph.validator(self.state):
            return self.path


        for edge, state in self.graph.substates(self.state, self.graph.objects):
            cost, rob_movements = self.graph.cost_calculator(state, self.path, [edge], self.movements)
            child = Node(self.graph, self.path + [edge], state, cost, rob_movements)
            self.graph.queue.push(child)

    def __repr__(self):
        return "Node(%s, %s)" % (self.path, self.state)



class PriorityQueue(object):
   
    def __init__(self): 
        self.heap = []
        self.counter = itertools.count()
        

    def pop(self):
        cost, count, node = heapq.heappop(self.heap)
        return node

    def push(self, node):
        count = next(self.counter)
        element = [node.cost, count, node]
        heapq.heappush(self.heap, element)

    def is_empty(self):
        return not self.heap





