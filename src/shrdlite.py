#!/usr/bin/env python

# Test from the command line:
# python shrdlite.py < ../examples/medium.json

from __future__ import print_function
from functools import partial
from copy import deepcopy

import sys
import json
import os
import random

from search_graph import SearchGraph
#TODO: Remove these imports after merging with master
from pdb import set_trace
from pprint import pprint


# Make a filepath that is independent of being called from src or test
GRAMMAR_FILE = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'shrdlite_grammar.fcfg')

# IMPORTANT NOTE:
#
# If you are using NLTK 2.0b9 (which is the one that is installed
# by the standard Ubuntu repository), then nltk.FeatureChartParser
# (in the parse function) fails to parse some sentences! In this
# case you can use nltk.FeatureTopDownChartParser instead.
# You can check if it is working by calling this:
#
#   python shrdlite.py < ../examples/small.json
#
# The program should return "Ambiguity error!". If it instead
# returns "Parse error!", then the NLTK parser is not correct,
# and you should change to nltk.FeatureTopDownChartParser instead.

def get_tree_label(result):
    """Returns the label of a NLTK Tree"""
    try:
        # First we try with NLTKv3, the .label() method:
        return result.label()
    except AttributeError, TypeError:
        # If that doesn't work we try with NLTKv2, the .node attribute:
        return result.node

def get_all_parses(parser, utterance):
    """Returns a sequence of all parse trees of an utterance"""
    try:
        # First we try with NLTKv2, the .nbest_parse() method:
        return parser.nbest_parse(utterance)
    except AttributeError, TypeError:
        try:
            # Then we try with NLTKv3, the .parse_all() method:
            return parser.parse_all(utterance)
        except AttributeError, TypeError:
            # Finally we try with NLTKv3, the .parse() method:
            return parser.parse(utterance)

def parse(utterance):
    import nltk
    grammar = nltk.data.load("file:" + GRAMMAR_FILE, cache=False)
    parser = nltk.FeatureChartParser(grammar)
    try:
        return [get_tree_label(result)['sem']
                for result in get_all_parses(parser, utterance)]
    except ValueError:
        return []


def possible_interpretations(tree, world, holding, objects):
    if tree[0] == "take" or tree[0] == "find":
        subjectives = find_objects(tree[1], world, objects)
        return (tree[0], subjectives)
    else:
        if len(tree) < 3:
            if holding:
                subjectives = [ holding ]
            else:
                return [] # "It" is referenced without anything in holding.
        else:
            subjectives = find_objects(tree[1], world, objects)

        dative = get_dative(tree)
        datives = find_objects(dative, world, objects)

    return (tree[-1][1], subjectives, datives)


def find_object(name, world):
    """ Finds an object in the world with the name, returns a tuple (x, y) """

    if "floor" in name:
        return ( int(name.split("-")[-1]), -1 )

    for x, column in enumerate(world):
        for y, obj in enumerate(column):
            if obj == name:
                return (x, y)

    raise KeyError("The object: %s is not in the world." % name)


def find_objects(entity, world, objects):
    """Takes a description of an entity, a world, and a hash of objects
       and returns a list of the matching objects ids."""

    desc = "floor" if entity == "floor" else entity[2]

    matching = []

    for x in range(0, len(world)):
        for y in range(-1, len(world[x])):
            obj = object_at(x, y, world, objects)
            if matches_entity(obj, entity, world, objects):
                matching.append(obj)

    return matching


def matches_description(obj, desc):
    """Compares an object with an object description."""
    if obj is None:
        return False

    if 'floor' in str(desc) or 'floor' in str(obj):
        return 'floor' in str(desc) and 'floor' in str(obj)

    if ((desc[1] == 'anyform' or desc[1] == obj['form'])
    and (desc[2] == '-' or desc[2] == obj['size'])
    and (desc[3] == '-' or desc[3] == obj['color'])):
        return True
    else:
        return False


def matches_entity(obj, entity, world, objects):

    if 'floor' in obj or 'floor' in entity:
        return 'floor' in obj and 'floor' in entity

    try:
        obj_desc = objects[obj]
    except KeyError:
        obj_desc = obj

    match = matches_description(obj_desc, entity[2])

    if entity[0] == 'relative_entity' and match:
        relational_obj = getattr(sys.modules[globals()['__name__']], "objects_%s" % negate_relation(entity[3][1]))

        x, y = find_object(obj, world)

        for obj in relational_obj(x, y, world, objects):
            if matches_entity(obj, entity[3][2], world, objects):
                return True
        else:
            return False

    return match


def matches_location(x, y, (location_type, relation, entity), world, objects):
    """Makes sure that location x,y is properly located in relation to an entity."""
    try:
        # Get an appropriate generator function
        relational_obj = getattr(sys.modules[globals()['__name__']], "objects_%s" % negate_relation(relation))
    except NameError, e:
        print >> sys.stderr, "Unknown relation: %s." % relation
        return False

    for obj in relational_obj(x, y, world, objects):
        try:
            obj_desc = objects[obj]
        except KeyError:
            obj_desc = obj

        if matches_description(obj_desc, entity):
            return True

    return False

def in_relation_to(x, y, name, world, objects):

    # function dispatch ?? is it worth it
    # location = find_object(name, world)

    limit = len(world) - 1
    relation = []

    ontop = objects_ontop(x, y, world, objects)
    for obj in ontop:
        relation.append(('below the', obj))

    below = objects_below(x, y, world, objects)
    for obj in below:
        relation.append(('ontop of', obj))

    right = objects_rightof(x, y, world, objects)
    for obj in right:
        relation.append(('to the left of', obj))

    left = objects_leftof(x, y, world, objects)
    for obj in left:
        relation.append(('to the right of', obj))

    return relation


def negate_relation(relation):
    if 'ontop' == relation:
        return 'below'
    elif 'inside' == relation:
        return 'below'
    elif 'above' == relation:
        return 'under'
    elif 'under' == relation:
        return 'above'
    elif 'leftof' == relation:
        return 'rightof'
    elif 'rightof' == relation:
        return 'leftof'
    else:
        return relation



# 'objects_*' functions are generators returning objects related to the x,y coordinates

def objects_ontop(x, y, world, objects):
    yield object_at(x, y+1, world, objects)

def objects_below(x, y, world, objects):
    yield object_at(x, y-1, world, objects)

def objects_inside(x, y, world, objects):
    for obj in objects_ontop(x, y, world, objects): yield obj

def objects_above(x, y, world, objects):
    return ( object_at(x, ypos, world, objects) for ypos in range(y+1, len(world[x])) )

def objects_under(x, y, world, objects):
    return ( object_at(x, ypos, world, objects) for ypos in range(-1, y) )

def objects_beside(x, y, world, objects):
    for obj in objects_leftof(x, y, world, objects): yield obj
    for obj in objects_rightof(x, y, world, objects): yield obj

def objects_leftof(x, y, world, objects):
    for obj in objects_in_column(x-1, world, objects): yield obj

def objects_rightof(x, y, world, objects):
    for obj in objects_in_column(x+1, world, objects): yield obj

def objects_in_column(x, world, objects):
    try:
        return ( object_at(x, y, world, objects) for y in range(-1, len(world[x])) if x >= 0 and x < len(world))
    except Exception, e:
        return ( x for x in [] )

def object_at(x, y, world, objects):
    """Returns the object at x,y if it exists in the world, otherwise None."""
    if y < 0:
        return 'floor-' + str(x)
    else:
        obj = None
        try: obj = world[x][y]
        except: pass
        return obj

def get_object_prop(name, objects):
    if not name:
        return {}
    elif "floor" in name:
        return name
    else:
        return objects[name]


def interpret(trees, world, holding, objects):
    interpretations = []
    trees_with_interpretation = []

    for tree in trees:
        interpretation = possible_interpretations(tree, world, holding, objects)

        if (interpretation and interpretation[1] and
           (len(interpretation) <= 2 or interpretation[2])):
            trees_with_interpretation += [tree]
            interpretations += [interpretation]

    # Make sure we have a single parse tree
    if not trees_with_interpretation:
        return []
    elif len(trees_with_interpretation) > 1:
        raise AmbiguityError(construct_ambiguity_question(trees_with_interpretation), {
            'trees': trees_with_interpretation,
            'interpretations': interpretations
            })

    # Make sure we have at least one interpretation
    if not interpretations:
        return []

    valid_tree = trees_with_interpretation[0]
    relation = interpretations[0][0]
    subjectives = interpretations[0][1]
    hasDatives = len(interpretations[0]) > 2
    if hasDatives:
        datives = interpretations[0][2]

    # filter goals according to cardinality
    subjective = get_subjective(valid_tree)
    subj_card = get_cardinality(subjective)

    dative = get_dative(valid_tree)
    dat_card = get_cardinality(dative)


    # Check "the" for subjective
    if "the" == subj_card and len(subjectives) > 1:
        raise AmbiguityError("There are many %s, which one did you mean?" % make_human_readable(subjective, True))

    # Check "the" for dative
    if "the" == dat_card and len(datives) > 1:
        raise AmbiguityError("There are many %s, which one did you mean?" % make_human_readable(dative, True))

    goals = []

    if hasDatives:
        goals += [(relation, subj, dat) for subj in subjectives for dat in datives if subj != dat]
    else:
        goals += [(relation, subj) for subj in subjectives]

    # filter out invalid goals
    goals = [goal for goal in goals if valid_goal(goal, objects)]

    comparator = partial(goal_difficulty_score, objects, holding, world)
    goals = sorted(goals, key=comparator)

    if "any" == subj_card:
        goals = filter_duplicates(goals, 1)

    if hasDatives and "any" == dat_card:
        goals = filter_duplicates(goals, 2)

    return goals


def filter_duplicates(iterable, column):
    if not iterable:
        return []

    item = iterable[0][column]
    new_iterable = [iterable[0]]

    for row in iterable[1:]:
        if row[column] == item:
            new_iterable.append(row)

    return new_iterable


def goal_difficulty_score(objects, holding, world, goal):
    score = 0
    score += get_heuristics(goal, (holding, world))

    if validate_state(goal, objects, (holding, world)):
        score += 1000

    return score


def get_subjective(tree):
    """ Trys to get the subjective from the command.
        returns None if no subjective is present (implying "it")."""
    if  'floor' == tree[1] or 'entity' in tree[1][0]:
        return tree[1]

def get_dative(tree):
    if 'relative' == tree[-1][0]:
        return tree[-1][2]

def get_cardinality(obj):
    if obj:
        if "floor" == obj:
            return "any"
        else:
            return obj[1]
    else: # "it" implied
        return ''

def construct_ambiguity_question(trees):
    query = ""

    for (i, tree) in enumerate(trees):
        query += " \"%s\"" % make_human_readable(tree[1]) # subjective in command
        if i < len(trees) - 1:
            query += " or"

    return "Did you mean%s?" % query


def make_human_readable(entity, plural=False):
    if "floor" in entity:
        return "the floor"

    if entity[0] == "basic_entity":
        return make_human_readable_object(entity[2], plural)
    else:
        return  make_human_readable_object(entity[2], plural) +\
                " %s " % entity[3][1] +\
                make_human_readable(entity[3][2])


def make_human_readable_object(obj, plural=False):
    es = ""

    if not plural:
        es = "a"

    es += " " + obj[2] if obj[2] != "-" else "" # size
    es += " " + obj[3] if obj[3] != "-" else "" # color
    es += " " + obj[1] if obj[1] != "anyform" else " object" # shape

    if es and plural:
        es += "s"

    #TODO: Create a function that switches "a" to "an" when appropriate.
    if es == "a object":
        if plural:
            return "objects"
        else:
            return "an object"

    return es

def make_human_readable_find_answer(relation, trees, objects):

    # The large white ball lies next to ..........
    # Use human_readable for the first part
    # Then access objects to translate the letters to names

    string = "Success! The object you are looking for lies "
    lenght = len(relation)
    trimmed = []
    counter = 0

    for x in range (0, lenght):
        p2 = relation[x][1]
        if "floor" in str(p2):
            if relation[x][0] == 'ontop of':
                string += 'directly on the floor'
                counter += 1
            else:
                pass
        elif p2 == None:
            pass
        else:
            trimmed.append(relation[x])

    lenght = len(trimmed)

    for x in range (0, lenght):
        p1 = trimmed[x][0]
        p2 = trimmed[x][1]
        if x == (lenght - 1):
            if counter != 0:
                string += " and " + p1 + " " + name_retrieve(objects, p2) + "!"
            else:
                string += p1 + " " + name_retrieve(objects, p2) + "!"
        else:
            if counter != 0:
                string += ", "
            string += p1 + " " + name_retrieve(objects, p2)
            counter += 1

    if lenght == 0:
        string += "directly on the floor without any adjacent obejcts!"

    return string

def name_retrieve(objects, position):
    return "a " + str(objects[position]["size"]) + " " + str(objects[position]["color"]) + " "  + str(objects[position]["form"])

def resolve_cardinality_ambiguity(goals, world, objects):
    #if desc[1] == 'the' or desc[1] == 'any':
    #    matching = matching[0:1] #TODO: Ask clarification if 'the' and len(matching) > 1

    if goals: return goals[0]


def solve(goals, world, holding, objects):
    goal = goals[0] #TODO: Handle several goals gracefully

    # add something that makes the output describe where the object is
    # also make the result['plan'] contain something so there will be no fault.
    validator = partial(validate_state, goal, objects)
    cost_calculator = partial(calculate_cost, goal)
    graph = SearchGraph(validator, child_states, cost_calculator, objects)
    graph.add_root_state((holding, world))
    path = graph.search()
    if path is None:
        raise Exception("Planner returned no path.")
    else:
        return ["%s %s" % t for t in path]

def solve_find(goals, world, holding, objects):
    goal = goals[0]
    position = find_object(goal[1],world)
    return in_relation_to(position[0], position[1], goal[1], world, objects)

def main(utterance, world, holding, objects, **_):
    result = {}
    result['utterance'] = utterance

    try:
        trees = _['state']['trees']
    except KeyError:
        trees = parse(utterance)
        result['trees'] = [str(t) for t in trees]
    if not trees:
        result['output'] = "Say again?"
        return result

    try:
        result['goals'] = goals = interpret(trees, world, holding, objects)
    except AmbiguityError, e:
        result['output'] = e.message
        result['state'] = e.state
        return result

    if not goals:
        result['output'] = "You're not making any sense."
        return result

    if goals[0][0] == 'find':
        result['plan'] = []
        relation = solve_find(goals, world, holding, objects)
        result['output'] = make_human_readable_find_answer(relation, trees, objects)
    else:
        try:
            result['plan'] = plan = solve(goals, world, holding, objects)
        except Exception:
            plan = None

        if not plan:
            if validate_state(goals[0], objects, (holding, world)):
                result['output'] = "The solution is already solved!"
            else:
                result['output'] = "I'm pretty sure that can't be done."

            return result
        result['output'] = "I figured it out!"
    return result


class AmbiguityError(Exception):
    """A custom error which can hold an optional state."""
    def __init__(self, message, state={}):
        super(AmbiguityError, self).__init__(message)
        self.state = state

    def __repr__(self):
        return "AmbiguityError(%s, %s)" % (self.message, self.state)


### Helper methods for the SearchGraph

def validate_state(goal, objects, (holding, world)):
    if goal[0] == 'take':
        return holding == goal[1]
    elif goal[0] == 'find':
        return True
    elif goal[0] == 'drop':
        rel_objects = objects_in_column(goal[2], world, objects)
    else:
        try: x, y = find_object(goal[2], world)
        except KeyError: return False
        f = getattr(sys.modules[globals()['__name__']], "objects_%s" % goal[0])
        rel_objects = f(x, y, world, objects)

    for obj in rel_objects:
        if obj == goal[1]:
            return True
    return False


def child_states((holding, world), objects):
    states = []

    for action in get_actions(holding, world, objects):
        states += [( action, apply_action(action, holding, world) )]

    return states


def apply_action(action, holding, world):
    "Modifies the state (world, holding) according to the action."
    h = deepcopy(holding)
    w = deepcopy(world)

    if action[0] == 'pick':
        if h: raise Exception("Trying to pick up something when already holding an object.")
        h = w[ action[1] ].pop()
    else: # drop
        if not h: raise Exception("Trying to drop something when not holding an object.")
        w[action[1]].append(h)
        h = None

    return (h, w)


def get_actions(holding, world, objects):
    if holding is None:
        for x, column in enumerate(world):
            if column:
                yield ("pick", x)
    else:
        for x in range(0, len(world)):
            actionGoal = ("drop", holding, "floor")
            if(len(world[x]) > 0):
                actionGoal = ("drop", holding, world[x][len(world[x]) - 1])

            if valid_goal(actionGoal, objects):
                yield ("drop", x)


def valid_goal(goal, objects):
    if len(goal) < 3:
        return True
    elif (goal[0] != 'inside' and goal[0] != 'ontop' and goal[0] != 'drop'):
        return True
    elif 'floor' in goal[2]:
        return True

    obj1 = objects[goal[1]]
    obj2 = objects[goal[2]]

     ## Balls must be in boxes or on the floor, otherwise they roll away.
    if obj1['form'] == 'ball' and obj2['form'] != 'box':
        return False

    ## Balls cannot support anything.
    elif obj2['form'] == 'ball':
        return False

    ## Small objects cannot support large objects.
    elif obj1['size'] == 'large' and obj2['size'] == 'small':
        return False

    ## Boxes cannot contain pyramids of the same size.
    elif obj1['form'] == 'pyramid' and obj2['form'] == 'box':
        if obj1['size'] == obj2['size']:
            return False

    ## Boxes cannot contain planks of the same size.
    elif obj1['form'] == 'plank' and obj2['form'] == 'box':
        if obj1['size'] == obj2['size']:
            return False
        elif string_bigger_than(obj1['size'], obj2['size']):
            return False

    ## Boxes can only be supported by tables of the same size
    elif obj1['form'] == 'box' and obj2['form'] == 'table':
        if obj1['size'] != obj2['size']:
            return False

    ## Boxes can only be supported by planks of the same size
    elif obj1['form'] == 'box' and obj2['form'] == 'plank':
        if obj1['size'] != obj2['size']:
            return False

    ## large boxes can only be supported by large bricks
    elif obj1['form'] == 'box' and obj2['form'] == 'brick':
        if obj1['size'] == 'large' and obj1['size'] != obj2['size']:
            return False

    return True


def string_bigger_than(a, b):
    if(a == b):
        return False
    elif(a == "small"):
        return False
    elif(a == "medium" and b == "large"):
        return False
    return True

def string_less_than(a, b):
    if(a == b):
        return False
    elif(a == "large"):
        return False
    elif(a == "medium" and b == "small"):
        return False
    return True

def string_equal(a, b):
    if(a == b):
        return True
    else:
        return False

def calculate_cost(goal, state, path, action, movements):
    heuristics = get_heuristics(goal,state)
    movement_cost = 0
    latest_movement = [0, 0]
    if(path):
        #Movement cost for the robot arm in the y axis
        latest_movement = path[len(path)-1]
        movement_cost = abs(latest_movement[1] - action[0][1]) + movements
    actual_cost = len(path) + 1
    return actual_cost + heuristics + movement_cost, movement_cost





def get_heuristics(goal, (holding, world)):
    score = 0

    if len(goal) == 3:
        try:
            ax, ay = find_object(goal[1], world)
            bx, by = find_object(goal[2], world)
            score = abs(ax - bx)
        except KeyError, e:
            score = 0



    obj=goal[1]
    if(goal[0] == "above"):
        return score + heuristics_above(goal, world, holding)
    elif goal[0] == 'take':
        return heuristics_take(goal, world, holding)
    elif goal[0] == "ontop":
        return score + heuristics_ontop(goal, world, holding)
    elif goal[0] == 'drop':
        return 1
    elif goal[0] == "beside":
        return heuristics_beside(goal,world, holding)
    elif goal[0] == "leftof":
        if score != 0:
            if ax < bx:
                score -= 1
            else:
                score += 1

        return score + heuristics_leftof(goal, world, holding)
    elif goal[0] == "rightof":
        if score != 0:
            if ax < bx:
                score += 1
            else:
                score -= 1

        reverse_goal = ("leftof", goal[2], goal[1])
        return heuristics_leftof(reverse_goal, world, holding)
    elif goal[0] == "below":
        goal_reverse = ("ontop", goal[2], goal[1])
        return score + heuristics_above(goal_reverse, world, holding)
    elif goal[0] == 'inside':
        return score + heuristics_ontop(goal, world, holding)
    elif goal[0] == 'under':
        goal_reversed = ("above", goal[2], goal[1])
        return score + heuristics_above(goal_reversed, world, holding)
    elif goal[0] == 'find':
        return 1
    else:
        raise Exception("Relation not supported")



def heuristics_above(goal, world, holding):
    obj = goal[1]
    rel_obj = goal[2]
    relation = goal[0]
    drop = 0
    if holding:
        drop = 1
        if holding == obj:
            return 1
        elif holding == rel_obj:
            obj_x, obj_y = find_object(obj, world)
            return (objects_above(obj_x, obj_y, world) +1 ) * 2 + drop
        else:
            drop = 1

    obj_x, obj_y = find_object(obj, world)
    rel_x, rel_y = find_object(rel_obj, world)
    if obj_x == rel_x:
        if obj_y < rel_y:
            return (objects_above(obj_x, obj_y, world) +1) * 2 + drop
        else:
            return 0
    else:
        return ( objects_above(obj_x, obj_y, world) +1 ) * 2 + drop



def heuristics_leftof(goal, world, holding):
    obj = goal[1]
    rel_obj = goal[2]
    drop = 0
    if holding:
        if holding == rel_obj:
            obj_x, obj_y = find_object(obj, world)
            if(obj_x != len(world)-1):
                return 1
            else:
                return (objects_above(obj_x, obj_y, world) +1) * 2 +1
        elif holding == obj:
            rel_x, rel_y = find_object(rel_obj, world)
            if rel_x != 0:
                return 1
            else:
                return (objects_above(rel_x, rel_y, world) +1) * 2 +1
        else:
            drop = 1

    obj_x, obj_y = find_object(obj, world)
    rel_x, rel_y = find_object(rel_obj, world)
    if obj_x +1 == rel_x:
        return 0
    if obj_x == rel_x:
        if obj_x == 0:
            return (objects_above(rel_x, rel_y, world) + 1) * 2 + drop
        else:
            if rel_y > obj_y:
                return (objects_above(rel_x, rel_y, world) + 1) * 2 + drop
            else:
                return (objects_above(obj_x, obj_y, world) + 1) * 2 + drop
    else:
        steps = 0
        if obj_x == len(world)-1:
            steps = steps + (( objects_above(obj_x, obj_y, world) +1 ) *2 )
        if rel_x == 0:
            steps = steps + ((objects_above(rel_x, rel_y, world) +1 ) *2 )
        if steps > 0:
            return steps + drop
        else:
            min_above = min(objects_above(obj_x,obj_y,world), objects_above(rel_x, rel_y, world))
            return (min_above + 1) * 2  + drop



def heuristics_take(goal, world, holding):
    obj = goal[1]
    extra_steps = 1
    if(holding):
        if(holding == obj):
            return 0
        else:
            extra_steps += 2

    obj_x, obj_y = find_object(obj, world)
    return (objects_above(obj_x, obj_y, world) * 2) + extra_steps


def heuristics_ontop(goal, world, holding):
    obj = goal[1]
    rel_obj = goal[2]
    drop = 0
    if holding:
        drop = 1
        if holding == obj:
            rel_x, rel_y = find_object(rel_obj, world)
            return (objects_above(rel_x, rel_y, world) +1) *2  + drop
        elif holding == rel_obj:
            obj_x, obj_y = find_object(obj, world)
            return (objects_above(obj_x, obj_x, world) +1) * 2 + drop
    obj_x, obj_y = find_object(obj, world)
    rel_x, rel_y = find_object(rel_obj, world)
    if obj_x == rel_x:
        if obj_y < rel_y:
            return (objects_above(obj_x, obj_y, world) + 1 ) * 2 + drop
        else:
            if(obj_y -1 == rel_y):
                return 0
            else:
                return (objects_above(rel_x, rel_y, world) ) * 2 + 2 + drop
    else:
        above_obj = objects_above(obj_x, obj_y, world)
        above_rel = objects_above(rel_x, rel_y, world)
        return (above_rel + above_obj + 1) * 2 + drop


def heuristics_beside(goal, world, holding):
    obj = goal[1]
    rel_obj = goal[2]
    drop = 0
    if(holding):
        if(holding == obj or holding == rel_obj):
            return 1
        else:
            drop = 1
    obj_x, obj_y = find_object(obj, world)
    rel_x, rel_y = find_object(rel_obj, world)
    if rel_x + 1 == obj_x or rel_x -1 == obj_x:
        return 0
    min_steps = min(objects_above(obj_x,obj_y,world), objects_above(rel_x, rel_y, world))
    return (min_steps + 1) * 2 + drop


def objects_above(obj_x, obj_y, world):
    objects_in_columnn = len(world[obj_x])
    if(obj_y + 1 == objects_in_columnn):
        return 0
    else:
        return objects_in_columnn - obj_y - 1




### Main

if __name__ == '__main__':
    input = json.load(sys.stdin)
    output = main(**input)
    # json.dump(output, sys.stdout)
    # json.dump(output, sys.stdout, sort_keys=True, indent=4)
    print("{", ",\n  ".join('%s: %s' % (json.dumps(k), json.dumps(v))
                            for (k, v) in output.items()), "}")
