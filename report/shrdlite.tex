%
% File eacl2014.tex
%
% Contact g.bouma@rug.nl yannick.parmentier@univ-orleans.fr
%
% Based on the instruction file for ACL 2013
% which in turns was based on the instruction files for previous
% ACL and EACL conferences

%% Based on the instruction file for EACL 2006 by Eneko Agirre and Sergi Balari
%% and that of ACL 2008 by Joakim Nivre and Noah Smith

\documentclass[11pt]{article}
\usepackage{eacl2014}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[swedish]{babel}
\usepackage{listings}
\usepackage{graphicx}
\special{papersize=210mm,297mm} % to avoid having to use "-t a4" with dvips
%\setlength\titlebox{6.5cm}  % You can expand the title box if you really have to

\lstset{captionpos=b, tabsize=2, basicstyle=\ttfamily, columns=fixed} % listing options


% Latex Snippets
% ==============

% # Code listings

% \lstinline[language=Python]{("ontop", "e", "f")}

% Use \pyline{<code>} instead:
\newcommand{\pyline}[1]{\lstinline[language=Python]{#1} }

% \begin{lstlisting}[language=Python, caption={This is the caption.}]
%     ('ontop', 'e', 'f')
% \end{lstlisting}


\title{Shrdlite}

\author{
  Gabre, Robin \\
  {\tt robijoha@dstudent.chalmers.se} \And
  Gabrielsson, William \\
  {\tt w.gabrielsson@gmail.com} \AND
  Ström, Daniel \\
  {\tt stromd@student.chalmers.se} \And
  Treptow, Anders \\
  {\tt treptow@student.chalmers.se} }

\date{\today}

\begin{document}

\maketitle

\begin{abstract}
This document details the project participants work on and implementation of
a natural language parser and planner in the spirit of Terry Winograd's SHRDLU
\cite{Win:71}.
\end{abstract}


\section{Introduction}

During the course Artificial Intelligence TIN172 given at Chalmers University of
Technology our group was tasked with creating an implementation for handling a
dialog system for controlling a robot that lives in a virtual block world. The
robot can move around objects of different forms, colors, and sizes. The robot
responds to linguistic input given by the user in a GUI and tries to reach the
state that it has interpreted from the user. The project was carried out during
the fourth study period.

The problem that we aimed to solve was how to get the robot arm to move objects
in the virtual world in accordance to the outcome that the user is expecting the
virtual world to have, or rather to reach the same final state as the user is
expecting. This involves first solving what the user means with the linguistic
input that the program receives, this is broken down into steps where the
program first needs to interpret the input given. Meaning that the program can
translate a statement into some form of data structure that the computer can
compute from. Next the program should be able to solve any ambiguities that may
exist with the given statement provided from the user. There may be more than
one possible final state from any utterance such as in the case of the utterance
`put the blue ball in a box on the floor' there may be several boxes that the
white ball can be placed in, it may also be that the white ball already is in a
box and that the ball itself should be placed on the floor, so the program needs
to resolve the ambiguity so that the program has one final state as a goal to
reach. Next it needs to plan how to reach this state. This involves planning how
to move the objects in the virtual world with whatever constraints that the
world might have in mind such as bigger objects can't be placed in smaller ones,
finding a path to the desired goal defined in the ambiguity resolution, and
construct a script of instructions that is sent to the GUI to perform to move
the robot arm and the objects on the screen.

\section{Interpretation}

After a user has sent a query to the AI, the query is parsed and the resulting
trees are sent on to be interpreted. The interpretation code goes through each
tree and produces one or several goals depending on the query. If several parse
trees make sense in the current world, the user will be asked to clarify his or
her intentions. If the query includes ``the'' but several possible objects are
discovered, the users will be asked to clarify the command. When any reasonable
solution is acceptable to the user, the AI will try to select the most efficient
one as determined by the heuristics.


\subsection{Finding all possible interpretations}
\label{subsec:interpretations}

The first step in the interpretation of a parse tree is to find out all possible
interpretations. An interpretation is the same as a goal and consists of a tuple
containing a relationship/command and one or two object ids. An example goal can
be seen in listing \ref{lst:goal}.

\begin{lstlisting}[language=Python, caption={The formats of a goal.}, label={lst:goal}]
        ('ontop', 'e', 'f')
        ('take', 'd')
\end{lstlisting}

To construct a goal, one begins by extracting the `relationship' from a
command (e.g. ontop, below, beside, etc.) or, when lacking a relationship, the
actual command (e.g. take, drop). The next thing is to find all objects in the
world that matches the entity description. For each object in the world the
description is traversed recursively and checked against the object properties.
The ids for each object is stored in a list together with the relationship. In
cases where two entities are part of a command, a separate list is constructed
for each. We call the first entity in a goal the ``subjective'' and the second
the ``dative''.

In the current version of the program, the object in `holding' is not matched
against the description. This means that the utterance ``pick up the white
ball'' followed by ``drop the white ball on the floor'' will produce an error.
One can however say ``drop it on the floor'' since `it' is considered to be
whatever is in holding. Another similar problem occurs with utterances like
``pick up the white ball'' followed by an utterance which depend on the function
\pyline{find_object()}. In our current implementation, when an object is held by
the arm the object is `removed' from the world. This should of course not be the
case, so a future improvement would be to include `holding' in the search as
well. Holding an object when stating queries which doesn't include the held
object still works.

\begin{figure}
    \label{fig:small_world}
    \includegraphics[width=0.48\textwidth]{img/small_world}
    \caption{The default layout in the small world.}
\end{figure}

This way of representing goals is straight-forward and follows exactly the
format in the project description. One could however argue that the
representation is too inflexible. Imagine the query ``put the white ball in a
red box on the floor'' in the small world shown in figure \ref{fig:small_world}.
In the small world, there is no red box on the floor so the interpretation will
fail, there is however a red box on the blue table. A better interpretation
might therefore be to first put the red box on the floor and then put the white
ball in the box. The main thing to change in order to achieve this behaviour is
to relax the representation of a goal by using a description of an object
instead of an id. A possible example might be seen in listing
\ref{lst:desc_goal}.

\begin{lstlisting}[language=Python, caption={A more dynamic goal format.}, label={lst:desc_goal}]
        ('inside',
         ('white', 'ball'),
         ('red', 'box'))
\end{lstlisting}

Implementing this kind of goal would entail simplifying the interpretation code,
since we only need to extract the relationship and the object descriptions, but
would complicate the planning. How would one, for instance, create a heuristic
when we're not sure what object to move and where? It might be, that a
stochastic search with greedy decent, such as simulated annealing, might be
simpler to implement, and about as effective.

\subsection{Handling cardinality and lazy interpretation}

The above procedure is done for each parse tree. Any tree that does not have any valid
interpretations in the given world are ignored. Given that a single valid parse
tree remains, the algorithm proceeds to check that we only have a single
subjective or dative if the `the' cardinality is used in the respective entity
description. Given that this is also fine, the next step of the interpretation
is to create all possible goals by combining the relationship with each
combination of the subjectives and datives. Pythons list comprehensions makes
this very easy:

\begin{lstlisting}[language=Python, caption={Constructing all possible goals
    using list comprehensions.}, label={lst:comprehensions}]
   [(relation, subj, dat)
    for subj in subjectives
    for dat in datives
    if subj != dat ]
\end{lstlisting}

When the list of goals has been constructed it is sorted according to the
heuristics function described in section \ref{subsec:heuristics},
with an added penalty if the goal is already valid in the world. The penalty
means that given the query ``pick up the ball'' when already holding a ball, the
AI will assume the user wants it to pick up a different ball than to simply say
it is finished. The sorting allows the interpreter to select the seemingly
easiest interpretation of a given query when the `any' cardinality is used. If
`any' is used for either the subjective or dative, the list is filtered to only
include goals of a single object for that entity. The filtering is greedy in
that it only looks at the single most efficient goal when deciding what object
to keep. One could imagine looking at the collective efficiency of keeping each
object instead as a possible improvement. Lastly, after the list is filtered,
any remaining goals are sent to the planning algorithms.


\subsection{Handling ambiguities}

When either a parse tree ambiguity or a cardinality ambiguity is detected, the
normal execution of the program is stopped, via an exception, and a question is
posed to the user asking for a clarification. The state of the parsing and
interpretation is also sent to the client and expected back together with the
clarifying statement.

The question posed to the user in the case of a tree ambiguity is constructed
by a function which takes a list of trees, extracts the subjective of each tree
and joins them into a string using `or' as a delimiter resulting in a string on
the form ``Did you mean X or Y or Z?''. The brunt of the work in constructing
the question is located in the function
\pyline{make_human_readable_object}
which goes through an object description and filters out all generic (i.e.
`-' or `anyform') values and concatenates the rest forming a readable
representation on the form
\pyline{size color shape}
given a full description.

The main benefit with using the parse tree for the construction of the question
is that the agent will use the same wording as the users. To exemplify, if the
users says ``put the white ball in a box on the floor'' the agent will ask
``Did you mean `a white ball' or `a white ball in a box'?''. Notice that the
agent does not specify the size of the ball needlessly.

In the case of a cardinality ambiguity, the
\pyline{make_human_readable_object}
function is reused but the default `plural' parameter is set to true. In this
case, the function does not add `a' in front of each object string but ends each
representation with `s'. In the case of the utterance ``Pick up the blue
object'', when several blue objects exists, this will result in the question
``There are several blue objects, which one did you mean?''.

Regardless of what question is asked, the logic for interpreting the answer
is unfortunately not implemented as of yet in the project.




\section{Planning}
\label{sec:planning}

Once a goal has been defined via the ambiguity resolution the goal, along with
the current state of the world is sent to the planner to solve. A goal is
defined as a tuple (See listing \ref{lst:goal}) of the desired relation that an object should
have towards the world. In order to find the path to the goal a graph is
constructed where each node in the graph represents a state of the world and the
root node of the graph represents the initial state of the world. Each path in
the graph represents an action that is taken from each node to reach the other.
The path search for our program uses a best-first and finds a least-cost path from the
root node to one goal node out of several possible goal nodes. This is achieved with
the A* algorithm which was implemented in our program which results in a worst case of:
\begin{equation}
O(|E|) = O(b^d)
\end{equation}

When a query is entered and the goal is already fulfilled in the world, there are two possible
outcomes. If there are one or more different solutions the program will make a
plan and execute it and return a success message. If the only solution is the
one that is already present in the world, the function which creates a search
graph will return nothing for the planner but will not terminate the function as
it would when there is no solution. This results in an error when there
actually is a solution but is handled in a wrongful way from \pyline{main()}.
Minor changes in the function \pyline{search()} in \pyline{search_graph.py} and
\pyline{main()} would resolve this error.

\subsection{Graph Searching Algorithm}

The graph that is constructed in order to run the search algorithm is composed of the root state, where each arc is an action that can be taken in order to reach another state. The graph traversal will thus result in that a sub-state can't reach the root again, meaning that this is an acyclic directed graph, specifically that is represented as a tree.
During the search, each node will validate whether or not a state upholds the
desired goal. If it does, it will return the path taken to the current state. If
the state can't be validated with the goal, then this node will create child
nodes based on the number of actions that can be performed from that state. A
list of actions is constructed from each possible outcome from the current
state. If the arm holds an object, then that object could be placed on any pile
to create a new state, if the arm doesn't hold any object, then a new state
would be created by picking up any object. During the construction of this list,
each action is filtered against the physical conditions of the world before
being returned as a possible action, such as a ball cannot be placed on anything
but the floor and a box.

The graph is constructed in the \pyline{SearchGraph} class, it is constructed by defining a function that is used to validate the state with the goal, a function that handles the creation of sub-states of a given state (finding possible actions, and returning the states from these actions), a dictionary containing the
definitions of each object in the world, the root state (the state of the world
before planning), and a function that calculates the heuristics to the goal
state. The nodes in the graph are defined in the Node class where the nodes keep
track of the path of actions that has been taken to reach the current node, the
state of the world for the node, the cost of the node in terms of the cost from
the path to that node, and the cost of the arm movement that leads up to that
state from the action. The \pyline{SearchGraph} class also consists of a sorted priority queue that is used to differentiate in what order the sub-states are validated and subsequently adding the sub-states of sub-states into the priority queue and this queue is sorted on the cost value for each node. 

During search the graph will pop the priority queue to get the node with the
least amount of cost. That node is then validated towards the goal and whether
or not it upholds the goal the node will either return the path or add it's
sub-states to the priority queue which will sort them with the other existing
nodes. This process is repeated until either the path to the state that upholds
the goal is found, or until all possible states have been validated and no state
upholds the goal.

\subsection{Physical constraints}

When each node evaluates what actions that can be taken for a state our implementation uses a function that returns all possible actions that are legal to take for that state. This function has two root actions that can be applied: pick, or drop. When the arm is not holding anything all legal actions consist of picking up the top object in each column that is not empty. If the arm is holding something, then in order for the arm to be able to drop that item on a column the object needs to be evaluated against the physical constraints that exist in the world. This is filtered by checking whether or not the object in the arm can be placed upon the specific object in each column respectively. The program evaluates whether or not an object is allowed to be placed in a column, e.g. a ball may not be placed on a table. The function to evaluate whether or not an action passes the filter is modularized so that the filtering can be exchanged. In order for this module to evaluate an action in the \pyline{get_actions} function that returns possible actions for each state needs to further define the action that needs the filter applied to. This is because the \pyline{valid_goal} function needs to fully define the full action all objects involved in the action against a dictionary of the objects in the world in order to fully evaluate whether it upholds the physical constraints in the world.

\begin{lstlisting}[language=Python, caption={Example of the format that is sent to filter against the physical constraints of the world.}, label={lst:pddlaction}]
   ("drop", "e", "g")
   ("drop", "e", "floor")
\end{lstlisting}

Since one of the assumptions towards the world is that the arm can pick up any object, any actions that deals with picking up objects will never be evaluated. In the case of dropping an object onto any other object the tuple in the listing above represents the action, the object that the arm is holding, and the object that it is trying to place it on.

Once a state has been validated the node returns the path to the goal state from the root state. This is represented in the form of a list of actions taken. The list of actions is a list of low level commands that is sent to the GUI in the following form:

\begin{lstlisting}[language=Python, caption={The format of the plan.}, label={lst:plan}]
   ['pick 1', 'drop 3']
\end{lstlisting}

Where the number represents what zero-indexed column that the action is to be applied to. The possibility to print a verbose variant of the plan exists, but is not included in the build.

In the GUI, there are 4 different worlds, each with various complexity in terms of how many objects there are in  the world, and the choice of objects (as some objects are not possible to stack on each other). Our implementation of the planner works in all 4 worlds, and produces the seemingly best solution to each problem. 

% TODO How well does the planner work with difficult worlds?

% TODO More good examples

% TODO More bad examples
% look into the solution for "put the red plank on the yellow brick, in the complex world. It seems to prefer to make small movements with the arm rather than moving a lot of stuff


\subsection{Heuristics} 
\label{subsec:heuristics} 
The heuristics for a particular state is defined by the sum of the minimum
number of actions,``pick'' or ``drop'' and the minumum number of arm movements
in x-axis, that the robot must do in order to reach the goal state. The physical
laws defined by the project description are excluded in order to simplify the
problem.

In short, the heuristics for object movements is found by counting the number
of objects above ``subjective'' and/or ``dative'',  depending on what the goal 
is and how the world in the current state looks like. Assume that the goal and
the world is as seen in listing \ref{lst:example_heuristics}.


\begin{lstlisting}[language=Python, caption={Example of a goal in a world},
label={lst:example_heuristics}]
goal = ('ontop', 'g', 'm')
world = [["e"],["g","l"],
        [],["k","m","f"],[]],
\end{lstlisting} In order to move ``g''
so it's on top of ``m'', the arm must first pick the objects that are above ``m''
and drop it to another column. Then it must repeat same procedure for the
objects that are above ``g'', and lastly move ``g'' so it's on top of ``m''. This
means that the heuristics for this state is given by:


\begin{lstlisting}[language=Python, caption={Example of a heuristic
    calculation}, label={lst:heuristic_calculation}]
    return (objects_above("g") +
    objects_above("m") + 1 ) * 2
\end{lstlisting}
Where ``+1'' is needed because the arm also have to move ``m'', furthermore
the multiplication is needed since each movements requires two actions
``pick'' and ``drop''.

Of course this calculation will not always hold, assume that the goal and world
looks as seen in listing \ref{lst:example_heuristics_1}.
\begin{lstlisting}[language=Python, caption={Example of a goal in a world},
label={lst:example_heuristics_1}]
    goal = ('ontop', 'g', 'm')
    world = [["e"],["g","m","l"],
             [],["k",f"],[]],
\end{lstlisting}

The goal is the same as the previous example, but ``g'' and ``m'' lies within
the same column. The calculation procedure seen in listing
\ref{lst:heuristic_calculation}
will not hold because it will count the object ``l'' twice. Instead the planner
first finds which of the objects that is located furthest below. Then it
calculates the objects that are above in the same manner as before. This is seen
in listing \ref{lst:heuristic_calculation_1}.
\begin{lstlisting}[language=Python, caption={Example of a heuristic
calculation}, label={lst:heuristic_calculation_1}]
    min = find_min("g", "m")
    return (objects_above(min) +1 )
             * 2
\end{lstlisting}

Another case worth some mention is when the ``dative'' is located
in the right- or the leftmost column, and the goal is to put the ``subjective''
to the right/left of the ``dative''. Assume the world looks like seen in
listing \ref{lst:example_heuristics_2}.
\begin{lstlisting}[language=Python, caption={Example of a goal in a world},
label={lst:example_heuristics_2}]
    goal = ('leftof', 'f', 'e')
    world = [["e"],["g","m","l"],
             [],["k",f"],[]],
\end{lstlisting}
The arm can't directly move the object ``f'' to the left of the object ``e''
because it would be outside the bounds of the world. There are two options
to this problem, the arm can either move ``e'' to another column so there is
space to the left and then move ``f'', or it can simply move ``e'' to the right
of the ``f'' and the goal is thus satisfied. The heuristic function will
use the second approach since it requires less movements.

Tests comparing a heuristic- to a non-heuristic planner apparently demonstrates
that the heuristic planner finds the optimal solution faster. As expected, both
planner implementations also delivers the same solution.  

For the state as it is now, there is no indications that the heuristic function
is not admissible. 

% TODO: Do we always know that the heuristics are admissible?

% TODO compare the results from the running the search without heuristics, and with.

\subsection{Lazy planner}
\label{subsec:lazyplanner}

Another feature that is
implemented is the lazy planner, meaning that the planner prefers shorter
movements in the x-axis. Looking at the actions along the paths, the planner is
then able to obtain a movement cost for the x-axis. One simple demonstration
regarding how the planner is lazy is illustrated in
figure~\ref{fig:lazy_example}.  Given the query ``put the green plank in the red
box'', the planner solves the problem by first placing the black ball on the
nearest available position and then put the blue box on the white brick and
finally put the green plank inside the red box. Thus the robot arm has performed
the request with a minimum number of movements in the x-axis.



\begin{figure}
    \label{fig:lazy_example}
    \includegraphics[width=0.48\textwidth]{img/lazy_merged}
    \caption{Demonstration of lazy planner.}
\end{figure}

The movement cost is achieved by looking at each actions along the path.
A path having actions ``((pick, 0), (drop, 3))'' will have a total
movement cost of 3. One problem with this solution is that the AI does not get
any information from the UI regarding the arm position. The planner assumes
that the initial arm position is at column 0. This means that arm movement cost
for the first action is often wrong. One way to solve this is to have a global
variable where the planner sets the position each time a problem is solved.
This is not implemented due to the wish that the AI should be stateless.

As explained in \ref{subsec:heuristics} under some circumstances an object can't
be placed at a certain location due to the fact that it doesn't exist in the
world. The lazy planner does however make plans where this solution is visible
even though other options are available. If the utterance is
``put the white ball left of the yellow box'' and the yellow box is located
in the leftmost corner, then the yellow box will be moved
instead of the white ball. Normally, the solution would be to move the white ball
to the yellow box but if the opposite solves the query then the least amount of
work is what is prioritized.


\section{Grammar}

The grammar that was provided with the pre-packaged code has been extended with
additional commands to give the program more functionality. The grammar now
includes the new command ``find'' and the program contains the extra functions
to handle the effects of that. The grammar has been extended with two new
features for this to work. First, the verb ``find'' is defined next to the other
basic actions, move and take. Secondly, an additional basic command under the
non-lexical grammar rules has been added to expand the command tree.

When a find query is inserted to the code, there are a few things that works
differently compared to the other basic commands. The parse and interpret
functions works like before but the solution to the problem is different. Since
the program doesn't need to move or take any objects to reach an acceptable
solution, an empty list is returned instead of a list with moves to make. This
is to indicate to the program that there is a solution but the solution in this
case is to do nothing. The reason to this is because the problem is solved in
the interpret function, and with the absence of the object the program is
looking for, no solution can be found. Instead, the solution to the problem is
merely a string which describes where the object is which yields no need to make
a plan for how to solve the problem.

To generate the result string the program calls a function which searches
through the column where to object is and the adjacent columns for other
objects. If any objects are found their ID will be translated into the
description of the object which can be found in the dictionary objects which is
in the world files, small.json etc.

The string that is generated includes the closest object, if any, above and
below the object and all objects on both sides. This will may result in a large
amount of text if there are lots of items next to the one the program is looking
for. Furthermore, if an object is located in a place where there are no adjacent
objects the program will solve the problem but the description might not help
out with locating the object, since the answer will be that it lies alone.

To use the new grammar functionality the user need to use the verb ``find''. To
give the grammar a larger vocabulary the verb has also been extended with
``where''``is'' and ``locate''. This won't make the solutions better but
does convey a more complex image to the user. The functionality works good in
most situations but can't find an object if the arm is holding the very object
it is looking for. This issue is discussed earlier in section
\ref{subsec:interpretations}

\section{Automatic testing}

Early on in the project, it was decided that automatic testing was to be used in
order to ensure that the project works with each committed change. The framework
used for this was python's \pyline{unittest2}. Besides the usual benefits with
automatic testing, an unforeseen, but welcomed, benefit of using unit testing was
that the tests provided us with a way to feed the example data to the program
without using the standard input. That left the input free to use for debugging
using the \pyline{pdb.set_trace()} command, which allowed us to stop execution
at specific points and run arbitrary code.


\bibliographystyle{acl}
\bibliography{shrdlite}


\clearpage

\appendix

\section{Individual Contributions}
\subsection{Gabre, Robin}
\begin{itemize}
    \item   Implemented more diversity to the grammar with the ``find'' command.
    \item   Adjusted the existing code to make it handle the new grammar
    \item   Implemented additional functions to collect information about
            adjacent objects and describing them.
\end{itemize}
\subsection{Gabrielsson, William}
\begin{itemize}
    \item   Implemented a custom priority queue 
            which sorts the nodes depending on their cost.
    \item   Implemented cost function that can be extended by alternative 
            costs, such as arm movements.
    \item   Implemented the heuristic functions for all possible states and goals.   
    \item   Implemented movement cost functions in order to make the planner lazy. 
\end{itemize}

\subsection{Ström, Daniel}
In chronological order:
\begin{itemize}
    \item   Set up of the project repository and unit testing.
    \item   Implemented the interpretation of parse trees.
    \item   Re-factored lots of code to be more efficient and readable.
    \item   Set up a general structure for ambiguity resolution without actually
            implementing anything.
    \item   Implemented a flexible graph searching scheme with binary search that
            can later easily be extended to A* by switching to a priority queue and
            implementing cost/heuristics functions.
    \item   Set up the initial Latex report code.
    \item   Implement a function that can parse an object description into a
            human-readable representation.
    \item   Ask the user what s/he meant when a parse tree ambiguity is detected.
    \item   Ask the user what s/he meant when a cardinality ambiguity is detected.
    \item   With ``any'': make the interpreter choose the most efficient
            interpretation (based on the heuristics).
    \item   Have the interpreter return several goals when using the
            ``all'' keyword.
    \item   Make the interpreter resolve object descriptions recursively.
    \item   Wrote about the interpreter and ambiguity resolution.
    \item   Corrected a slew of bugs.
\end{itemize}


\subsection{Treptow, Anders}
\begin{itemize}
	\item	Created lookup functions for finding objects in the world
	\item	Designed initial planner algorithm
	\item	Implemented the algorithms for determining possible goals each state can achieve in the planner
	\item	Created a way to validate whether a state upholds a desired goal
	\item	Implemented and integrated world constraints as a separate module into the planner
\end{itemize}
\end{document}
