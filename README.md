﻿Shrdlite
========

by Daniel Ström, Robin Gabre, Anders Treptow och William Gabrielsson


Current State of the Project
----------------------------

The project in its current state satisfies the basic requirements of the project, and then some.

*   The algorithms respect the constraints imposed by the physical laws.
*	Ambiguous queries are detected and a clarification question is posed to the user.
*   Interpretations are represented by goals in the form (relation, obj1, obj2).
*	All quantifiers work until the planning stage, from there on *all* is not supported.
* 	The interpretation engine uses the heuristics to choose lazy goals.
*	The planner is implemented using A*.
*	The planner uses a modified heuristic so the agent prefer quicker/easier actions.
*	A new command "find" (or "locate", or "where is") will locate an object and print a human readable
	location relative to surrounding objects.
*   Given the above, the algorithm works and produces a valid plan that can be used together with the web GUI.


Known Issues
--------------

# Ambiguity Resolution
*	Currently the agent ignores the reply from the user, instead expecting a new command.

# Heuristics
*   Since we can't initially know the position of the robot arm,
    we can't be completely lazy in actions such as drop or take 
    without changes to the GUI-code or some other state rememberance.
