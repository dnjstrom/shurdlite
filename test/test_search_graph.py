import unittest
import json
from pprint import pprint
from pdb import set_trace
import math

# import src-folder
import sys, os
parent = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'src'))
sys.path.insert(1, parent)

from search_graph import SearchGraph

# All tests suites in a package/folder can be run with the command:
#     python -m unittest discover
# A specific test can be run as the following example:
#     python test_search_graph.py TestShrdlite.test_small_parse
class TestShrdlite(unittest.TestCase):

    def setUp(self):
        example_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'examples'))

        with open(os.path.join(example_path, 'small.json')) as data_file:
            self.small = json.load(data_file)

        with open(os.path.join(example_path, 'medium.json')) as data_file:
            self.medium = json.load(data_file)


    @unittest.skip("Broken when A* was implemented.")
    def test_binary_tree(self):
        pi = [int(char) for char in str(math.pi) if char is not '.'] # [314159265359]

        validator = lambda sought: lambda (i,n): n == sought

        # substates treats the list of pi digits as a binary heap
        def substates((i, n)):
            states = []
            try: states.append( (2*i+1, (2*i+1, pi[2*i+1])) )
            except Exception: pass
            try: states.append( (2*i+2, (2*i+2, pi[2*i+2])) )
            except Exception: pass
            return states

        def search_for(x):
            graph = SearchGraph(validator(x), substates)
            graph.add_root_state((0,3))
            return graph.search()

        self.assertEqual([], search_for(3))
        self.assertEqual(None, search_for(13))
        self.assertEqual([1,3,7], search_for(6))
        self.assertEqual([2], search_for(4))
        self.assertEqual([2,5], search_for(9))


if __name__ == '__main__':
    unittest.main()
