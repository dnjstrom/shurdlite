import unittest
import json
from pprint import pprint
from pdb import set_trace

# import src-folder
import sys, os
parent = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'src'))
sys.path.insert(1, parent)

import shrdlite

# All tests suites in a package/folder can be run with the command:
#     python -m unittest discover
# A specific test can be run as the following example:
#     python test_shrdlite.py TestShrdlite.test_small_parse
class TestShrdlite(unittest.TestCase):

    def setUp(self):
        example_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'examples'))

        with open(os.path.join(example_path, 'small.json')) as data_file:
            self.small = json.load(data_file)

        with open(os.path.join(example_path, 'medium.json')) as data_file:
            self.medium = json.load(data_file)


    def test_small_parse(self):
        correct_parse = ('[(move, (basic_entity, the, (object, ball, -, white)), '
            '(relative, inside, (relative_entity, any, (object, box, -, -), '
            '(relative, ontop, floor)))), (move, (relative_entity, the, '
            '(object, ball, -, white), (relative, inside, (basic_entity, any, '
            '(object, box, -, -)))), (relative, ontop, floor))]')
        parse = str(shrdlite.parse(self.small["examples"][0]))
        self.assertEqual(parse, correct_parse)

    def test_small_question(self):
        utterance = "find the white ball".split()
        trees = shrdlite.parse(utterance)
        interpretations = shrdlite.interpret(
            trees,
            self.small["world"],
            None,
            self.small["objects"])
        self.assertEqual(interpretations, [('find', 'e')])

    def test_medium_parse(self):
        correct_parse = ('[(move, (relative_entity, the, (object, brick, -, -), '
            '(relative, leftof, (basic_entity, any, (object, pyramid, -, -)))), '
            '(relative, inside, (basic_entity, any, (object, box, -, -))))]')
        parse = str(shrdlite.parse(self.medium["examples"][0]))
        self.assertEqual(parse, correct_parse)

    def test_find_basic_objects(self):
        parse = shrdlite.parse(self.small["examples"][0])
        objects = shrdlite.find_objects(parse[0][1], #(basic_entity, the, (object, ball, -, white)
            self.small['world'],
            self.small['objects'])
        self.assertEqual(objects, ["e"])

    def test_find_relative_objects(self):
        parse = shrdlite.parse(self.small["examples"][0])
        objects = shrdlite.find_objects(parse[0][2][2], # (relative_entity, any, (object, box, -, -), (relative, ontop, floor))
            self.small['world'],
            self.small['objects'])
        self.assertEqual(objects, ["k"])

    def test_validate_state(self):
        is_valid = lambda goal: shrdlite.validate_state(goal,
            self.small["objects"], (self.small["holding"], self.small["world"]))

        self.assertEqual(is_valid(("take", "e")), False)
        self.assertEqual(is_valid(("ontop", "l", "g")), True)
        self.assertEqual(is_valid(("ontop", "g", "l")), False)
        self.assertEqual(is_valid(("ontop", "e", "l")), False)

    def test_valid_goal(self):
        def isValid(goal):
            return shrdlite.valid_goal(goal, self.small["objects"])

        self.assertFalse( isValid(('ontop', 'l', 'm')) ) # large in small
        self.assertFalse( isValid(('ontop', 'e', 'j')) ) # box on pyramid


    @unittest.skip("Not actually a test yet.")
    def test_integration(self):
        utterance = "put the white brick on the floor".split()
        plan = shrdlite.main(utterance,
                             self.medium["world"],
                             None,
                             self.medium["objects"])

    @unittest.skip("Not actually a test yet again.")
    def test_integration_find(self):
        utterance = "find the white ball".split()
        plan = shrdlite.main(utterance,
                             self.small["world"],
                             None,
                             self.small["objects"])

    def test_solve(self):
        plan = shrdlite.solve([('ontop', 'g', 'l')],
                              self.small["world"],
                              None,
                              self.small["objects"])

        correct = ['pick 1', 'drop 2', 'pick 1', 'drop 2']
        self.assertEqual(plan, correct)

    def test_actions(self):
        world = self.small["world"]
        objects = self.small["objects"]
        holding = self.small["holding"]
        holding = "l"
        goal = ("take", "e")


    def test_matches_entity(self):
        entity = ('basic_entity', 'any', ('object', 'ball', '-', '-'))
        isMatch = shrdlite.matches_entity('e', entity, self.small['world'], self.small['objects'])
        self.assertTrue(isMatch)


        entity = ('relative_entity',
            'the',
            ('object', 'ball', '-', '-'),
            ('relative', 'leftof', ('basic_entity', 'any', ('object', 'table', '-', '-'))) )
        isMatch = shrdlite.matches_entity('e', entity, self.small['world'], self.small['objects'])
        self.assertTrue(isMatch)


    def test_find_solve_relation(self):
        goal = []
        goal.append("find")
        goal.append("e")
        relation = shrdlite.solve_find([goal], self.small['world'], None, self.small['objects'])
        self.assertEqual(relation, [('below the', None),
            ('ontop of', 'floor-0'),
            ('to the left of', 'floor-1'),
            ('to the left of', u'g'),
            ('to the left of', u'l')])


    def test_heuristics_under(self):
        world = [["e"],["g","l"],[],["k","m","f"],[]]
        goal_under = ("under", "g", "k")
        goal_under_same = ("under", "l","g")
        goal_under_valid = ("under", "g","l")
        goal_under_holding = ("under", "a", "g")
        state = (None, world);
        state_holding = ("a", world);
        self.assertEqual(shrdlite.get_heuristics(goal_under, state), 6)
        self.assertEqual(shrdlite.get_heuristics(goal_under_same, state),4)
        self.assertEqual(shrdlite.get_heuristics(goal_under_same, ("a", world)),5)
        self.assertEqual(shrdlite.get_heuristics(goal_under_same, ("a", world)),5)

    def test_heuristics_leftof(self):
        world = [["e"],["g","l"],[],["k","m","f"],[]]
        goal_left = ("leftof", "m", "l")
        goal_left_outside = ("leftof", "g", "e")
        self.assertEqual(shrdlite.get_heuristics(goal_left,(None, world)), 2)
        self.assertEqual(shrdlite.get_heuristics(goal_left_outside, (None, world)), 2)


    def test_heuristics_rightof(self):
        world = [[],["g"],[],["k","m","f"],["e","l"]]
        goal_rightof = ("rightof", "g", "l")
        goal_right_outside = ("rightof", "e", "l")
        goal_right_holding = ("rightof", "a", "e")
        self.assertEqual(shrdlite.get_heuristics(goal_rightof, (None, world)), 2)
        self.assertEqual(shrdlite.get_heuristics(goal_right_outside, (None, world)), 2)
        self.assertEqual(shrdlite.get_heuristics(goal_right_holding, ("a", world)), 5)

    def test_heuristics_ontop(self):
        world = [[],["g"],[],["k","m","f"],["e","l"]]
        goal = ("ontop", "k", "e")
        goal_same = ("ontop", "k", "f")
        goal_same_1 = ("ontop", "f", "k")
        goal_valid = ("ontop", "f", "m")
        goal_holding = ("ontop", "a", "m")
        self.assertEqual(shrdlite.get_heuristics(goal, (None, world)), 8)
        self.assertEqual(shrdlite.get_heuristics(goal_same, (None, world)), 6)
        self.assertEqual(shrdlite.get_heuristics(goal_same_1, (None, world)), 6)
        self.assertEqual(shrdlite.get_heuristics(goal_valid, (None, world)), 0)
        self.assertEqual(shrdlite.get_heuristics(goal_same_1, ("a", world)), 7)
        self.assertEqual(shrdlite.get_heuristics(goal_holding, ("a", world)), 5)


    def test_heuristics_beside(self):
        world = [[],["g"],[],["k","m","f"],["e","l"]]
        goal = ("beside", "g", "e")
        goal_same = ("beside", "k", "m")
        goal_valid = ("beside", "f", "e")
        goal_holding = ("beside", "a", "e")
        self.assertEqual(shrdlite.get_heuristics(goal, (None, world)), 2)
        self.assertEqual(shrdlite.get_heuristics(goal_same, (None, world)), 4)
        self.assertEqual(shrdlite.get_heuristics(goal_valid, (None, world)), 0)
        self.assertEqual(shrdlite.get_heuristics(goal_holding, ("a", world)), 1)

    def test_heuristics_above(self):
        world = [[],["g"],[],["k","m","f"],["e","l"]]
        goal = ("above", "g", "k")
        goal_same = ("above", "k", "f")
        goal_holding = ("above", "e", "a")
        goal_valid = ("above", "f", "k")
        self.assertEqual(shrdlite.get_heuristics(goal, (None, world)), 2)
        self.assertEqual(shrdlite.get_heuristics(goal_same, (None, world)), 6)
        self.assertEqual(shrdlite.get_heuristics(goal_holding, ("a", world)), 5)
        self.assertEqual(shrdlite.get_heuristics(goal_valid, (None, world)), 0)

    def test_heuristics_under(self):
        world = [[],["g"],[],["k","m","f"],["e","l"]]
        goal = ("under", "g", "k")
        goal_same = ("under", "m", "k")
        goal_holding = ("under", "a", "e")
        goal_valid = ("under", "m", "f")
        self.assertEqual(shrdlite.get_heuristics(goal, (None, world)), 6)
        self.assertEqual(shrdlite.get_heuristics(goal_same, (None, world)), 6)
        self.assertEqual(shrdlite.get_heuristics(goal_holding, ("a", world)), 5)
        self.assertEqual(shrdlite.get_heuristics(goal_valid, (None, world)), 0)


    def test_preconditions(self):
        objects = self.medium["objects"]
        self.assertEqual(shrdlite.valid_goal(("drop", "e", "floor"), objects), True) #large ball on floor
        self.assertEqual(shrdlite.valid_goal(("drop", "c", "l"), objects), False) #large plank in large box
        self.assertEqual(shrdlite.valid_goal(("drop", "d", "k"), objects), True) #small plank in large box
        self.assertEqual(shrdlite.valid_goal(("drop", "e", "c"), objects), False) #large ball on large plank
        self.assertEqual(shrdlite.valid_goal(("drop", "l", "h"), objects), False) #large box on small table
        self.assertEqual(shrdlite.valid_goal(("drop", "k", "g"), objects), True) #large box on large table
        self.assertEqual(shrdlite.valid_goal(("drop", "a", "e"), objects), False) #large brick on large ball
        self.assertEqual(shrdlite.valid_goal(("drop", "i", "k"), objects), False) #large pyramid in large box
        self.assertEqual(shrdlite.valid_goal(("drop", "j", "k"), objects), True) #small pyramid in large box

    @unittest.skip("Not actually a test yet again. SearchGraph test")
    def test_solve_already_solved(self):
        utterance = "put the red box on a blue table".split()
        plan = shrdlite.main(utterance,
                             self.small["world"],
                             None,
                             self.small["objects"])
if __name__ == '__main__':
    unittest.main()
