import unittest
import json
from pprint import pprint
from pdb import set_trace

# import src-folder
import sys, os
parent = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'src'))
sys.path.insert(1, parent)

import shrdlite

# All tests suites in a package/folder can be run with the command:
#     python -m unittest discover
# A specific test can be run as the following example:
#     python test_shrdlite.py TestShrdlite.test_small_parse
class TestInterpret(unittest.TestCase):

    def setUp(self):
        example_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'examples'))

        with open(os.path.join(example_path, 'small.json')) as data_file:
            self.small = json.load(data_file)

        with open(os.path.join(example_path, 'medium.json')) as data_file:
            self.medium = json.load(data_file)


    # Test helpers

    def interpretation_helper(self, utterance, holding=None):
        trees = shrdlite.parse(utterance.split())
        return shrdlite.interpret(
            trees,
            self.small["world"],
            holding,
            self.small["objects"])

    def assert_length(self, utterance, length, holding=None):
        interpretations = self.interpretation_helper(utterance, holding)
        self.assertEqual(len(interpretations), length)


    # Actual tests

    def test_tree_ambiguity_single_solution(self):
        self.assert_length("put the white ball in a box on the floor", 1)

    def test_any(self):
        self.assert_length("pick up a blue object", 1)

    def test_any_complex(self):
        self.assert_length("put a ball in a box", 1)

    def test_left_right(self):
        interpretations = self.interpretation_helper("find the ball to the left of a table")
        self.assertEqual(len(interpretations), 1)

    def test_all(self):
        self.assert_length("put the white ball beside all blue objects", 2)

    def test_it(self):
        self.assert_length("drop it on the floor", 0)

    def test_drop(self):
        self.assert_length("drop it on the floor", 1, "p")

    def test_construct_question(self):
        trees = shrdlite.parse(self.small["examples"][0])
        question = shrdlite.construct_ambiguity_question(trees)
        self.assertEqual('Did you mean "a white ball" or "a white ball inside a box"?', question)

    def test_take(self):
        interpretations = self.interpretation_helper("pick up a blue object")
        interpretation_after = [('take', 'g')]
        self.assertEqual(interpretations, interpretation_after)

if __name__ == '__main__':
    unittest.main()
